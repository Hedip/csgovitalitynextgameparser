# CsgoVitalityNextGameParser

___

## A Simple HLTV.org scraper

This is a simple implementation of a webparser that can read the time for the next game of Team Vitality.

This code will host an API on port 80 with the endpoint `/vitality` that will output the date (if the match is not today) OR the time of their next match.

To install :
```
git clone git@gitlab.com:Hedip/csgovitalitynextgameparser.git

npm install

node .\index.js
```

___

### Licensing

Use this code as you wish. No licensing.