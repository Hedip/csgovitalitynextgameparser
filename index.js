const cheerio = require('cheerio');
const express = require('express');
var cors = require('cors')
const fetch = require("node-fetch");

const app = express();
app.use(cors())
const port = 80

const url = 'https://www.hltv.org/team/9565/vitality#tab-matchesBox';


async function main() {
    let data = await fetch(url, {headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/115.0'}});
    let text = await data.text();
    const $ = cheerio.load(text)

    const el = $('.table-container.match-table tbody td span')
    const dateTime = el[0].children[0].data;
    return dateTime
}

app.get('/vitality', async (req, res) => {
    let nextVitaGame = await main();
    res.json({data: nextVitaGame})
})

app.listen(port, () => {
    console.log('Server successfuly started.')
})